# Augmented Reality TIC-TAC-TOE

## Brief Description

This project implements an augmented reality version of Tic-Tac-Toe. In this game, the user draws an X on a paper board. Using a webcam and digital image processing techniques, the computer then determines the board state. Using this information, the computer chooses a move and displays the move in the appropriate place on the screen.

## Team Information

Team Pixel  
T.A – Eshan Gupta  
Sri Anvith Dosapati –2020102015(ECE)  
Surabhi Jain-2021121004(CSD)  
Bodgam Rohan Reddy – 2020102039(ECE)  
Rohan Gupta – 20202112022(ECD)  

## Installation commands

Download install Python Python 3.10.6
Install the dependecies using these instructions

```
pip install numpy
pip install opencv-python
pip install matplotlib
```

To run finally run the given below instructions

```
python3 ar_tictactoe.py
```

## Running Instructions

Point the camera towards
The Frame being captured should only contain the Tic-Tac-Toe board.
The intersection points should be emphasized.
Once all that is done, the player should make the first move on the board. The player is always the first one to move.
The player can only move X, else move won't be detected.
Once move is made, the computer detects the move and stores it in the game state.
If there is a change in the game state, the game engine comes up with a move.
The circle is then drawn over the frame using the acquired homography.
The game is played till the end and the winner is declared.

## Biblography:

Augmented Reality Tic-Tac-Toe,Joe Maguire, David Saltzman
Open cv - documentation https://docs.opencv.org/4.x/
