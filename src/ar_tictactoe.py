import cv2
import numpy as np
import matplotlib.pyplot as plt


"""
Game Logic
"""
# check the state of the game
def fin(board):
    winpos = [[0,1,2],[3,4,5],[6,7,8],[0,3,6],[1,4,7],[2,5,8],[0,4,8],[2,4,6]]
    for p in winpos:
        if(board[int(p[0]/3)][int(p[0]%3)] == 1 and board[int(p[1]/3)][int(p[1]%3)] == 1 and board[int(p[2]/3)][int(p[2]%3)] == 1):
            return -1
        elif(board[int(p[0]/3)][int(p[0]%3)] == 2 and board[int(p[1]/3)][int(p[1]%3)] == 2 and board[int(p[2]/3)][int(p[2]%3)] == 2):
            print(p)
            return 1
    if 0 not in board:
        return 2
    return 0  

# check if there is a win move for cpu
def cpuwinmove(i,board):
    b = board.copy()
    b[i] = 2
    winpos = [[0,1,2],[3,4,5],[6,7,8],[0,3,6],[1,4,7],[2,5,8],[0,4,8],[2,4,6]]
    for p in winpos:
        if(b[p[0]] == b[p[1]] == b[p[2]] == 2): 
            return True
    return False

# check if there is a win move for human to block it
def humanwinmove(i,board):
    b = board.copy()
    b[i] = 1
    winpos = [[0,1,2],[3,4,5],[6,7,8],[0,3,6],[1,4,7],[2,5,8],[0,4,8],[2,4,6]]
    for p in winpos:
        if(b[p[0]] == b[p[1]] == b[p[2]] == 1): 
            return True
    return False

# returns the next best cpu move
def computer_move(board):
    b = np.ravel(board)
    for i in range(len(b)):
        if(b[i] == 0 and cpuwinmove(i,b)):
            return i
    for i in range(len(b)):
        if(b[i] == 0 and humanwinmove(i,b)):
            return i
    for i in [4,0,8,2,6,1,3,5,7]:
        if(b[i] == 0):
            return i 

def nextmove(board):
    i = computer_move(board)
    nextboard = board.copy()
    nextboard[int(i/3)][int(i%3)] = 2
    return nextboard, i

# makes the cpu move and returns the gamestate and move made
def game(board):
    if fin(board)==2:
        return [],0,2
    if fin(board)==-1:
        return [],0,-1
    if fin(board)==0:
        board, i = nextmove(board)
        if fin(board)==1:
            return board,i,1
        else:
            return board, i, 0


# shi tomashi algorithm to find best representative points
def shi_tomashi(image):
    gray = image.copy()
    # using shi tomashi to get 4 corners
    corners = cv2.goodFeaturesToTrack(gray, 4, 0.01, 100)
    if corners is None:
        corners = [[0,0], [0,0], [0,0], [0,0]]
        return np.array(corners)
    corners = np.int0(corners)

    # makes sure that detected points A,B,C,D are in the correct order according to destination points
    corners = sorted(np.concatenate(corners).tolist())
    corners = sorted(corners, key=lambda x: (x[0]+x[1]))
    corners2 = []
    corners2.append(corners[0])
    if(corners[1][0]<corners[2][0]):
        corners2.append(corners[2])
        corners2.append(corners[1])
    else:
        corners2.append(corners[1])
        corners2.append(corners[2])
    corners2.append(corners[3])

    # label the detected points
    im = image.copy()
    for index, c in enumerate(corners2):
        x, y = c
        cv2.circle(im, (x, y), 3, 255, -1)
        character = chr(65 + index)
        cv2.putText(im, character, tuple(c), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 255), 2, cv2.LINE_AA)

    cv2.imshow('corners', im)
    return corners2

# using Hough transform to Detect 'X' in subimages
def Hough(img):
    lines_list =[]
    lines = cv2.HoughLinesP((255-img), 1, np.pi/180, threshold=10, minLineLength=5, maxLineGap=10)
    if lines is None:
        return 0
    for points in lines:
        x1,y1,x2,y2=points[0]
        lines_list.append([(x1,y1),(x2,y2)])
    # returns the number of lines/linepoints
    return len(lines_list)

# opencv camera feed
vid = cv2.VideoCapture(2)

# gamestate variables to check changes
circles = np.zeros(9)
game_state0 = np.zeros(9)
game_state1 = np.zeros(9)
prev_state = np.zeros(9)
stable_cnt = 0

CNT = 0
while(True):
    # capture a frame from camera
    ret, frame = vid.read()

    img = frame
    # convert it to BW
    img = cv2.cvtColor(img, cv2.COLOR_RGB2GRAY)

    # get the coordinates of actual intersection points
    corners = shi_tomashi(img)
    orig = img.copy()

    # coordinates of intersection points in perspective corrected 900x900 image
    destPoints = [[300,300], [300,600], [600,300], [600,600]]

    # calculating Homography matrix
    mat = cv2.getPerspectiveTransform(np.float32(corners), np.float32(destPoints))


    white_image = np.zeros((900, 900), np.uint8)
    white_image[:] = 255
    img = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C, cv2.THRESH_BINARY,25,2)
    kernel = np.ones((3, 3), np.uint8)
    img = cv2.dilate(img, kernel, iterations=2)
    img = cv2.erode(img, kernel, iterations=1)
    
    # warping the image to get perpective corrected
    warped = cv2.warpPerspective(img, mat, (900, 900), white_image, borderMode=cv2.BORDER_TRANSPARENT)
    
    # separate 900x900 image into 9 subimages of 300x300
    subs_list = []
    for i in range(3):
        for j in range(3):
            cur = warped[i*300:(i+1)*300, j*300:(j+1)*300]

            # cut 50px on all directions to remove grid lines
            cur = cur[50:250, 50:250]
            subs_list.append(cur)
    
    result=[]

    # get current state of the game
    for sub in subs_list:
        result.append(Hough(sub))
    ones = []
    for r in result:
        if r>20:
            ones.append(1)
        else:
            ones.append(0)

    ones = np.array(ones)
    
    cur_state = ones.copy()
    cur_state = np.add(cur_state, circles)

    # changing game state only once every 48 frames to account for noise or movement
    if np.allclose(prev_state, cur_state):
        stable_cnt += 1
    else:
        stable_cnt = 0

    if stable_cnt==48:
        game_state1 = cur_state.copy()
        stable_cnt = 0
    
    
    # call the game logic if the state is changed i.e if the player move is made
    if np.allclose(game_state0, game_state1)==False:
        board = np.array(game_state1).reshape(3, 3)
        gamestate1,new_o,res = game(board)
        game_state1[new_o] = 2
        game_state0 = game_state1.copy()
        if res==0:
            circles[new_o] = 2
        if res==-1:
            print("You Win\n")
            break
        if res==1:
            print("You Lose\n")
            break
        if res==2:
            print("Draw\n")
            break

    prev_state = cur_state.copy()

    # drawing circles according to game moves
    data = np.uint8([[0]*900]*900)
    for i in range(9):
        if circles[i]==2:
            cx = int((i%3))*300+150
            cy = int((i/3))*300+150
            data = cv2.circle(data, (cx, cy), 90, (1), 5)

    # warping the circles back to original perspective 
    warpedData = np.ones(img.shape)
    if CNT>48:
        warpedData = cv2.warpPerspective(data, np.linalg.inv(mat), (img.shape[1], img.shape[0]))
    cv2.imshow('frame', (1-data)*warped)
    cv2.imshow('res', orig*(1-warpedData))
    print(game_state1)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break
    if CNT<=200:
        CNT += 1
vid.release()
cv2.destroyAllWindows()