import cv2
import numpy as np
import matplotlib.pyplot as plt

def shi_tomashi(image):
    # gray = cv2.cvtColor(image, cv2.COLOR_RGB2GRAY)
    gray = image.copy()
    corners = cv2.goodFeaturesToTrack(gray, 4, 0.01, 100)
    corners = np.int0(corners)
    corners = sorted(np.concatenate(corners).tolist())
    print('\nThe corner points are...\n')

    im = image.copy()
    for index, c in enumerate(corners):
        x, y = c
        cv2.circle(im, (x, y), 3, 255, -1)
        character = chr(65 + index)
        print(character, ':', c)
        cv2.putText(im, character, tuple(c), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 255), 2, cv2.LINE_AA)

    plt.figure(2)
    plt.imshow(im)
    plt.title('Corner Detection: Shi-Tomashi')
    return corners

img = cv2.imread('test.png',0)

destPoints = [[300,300], [300,600], [600,300], [600,600]]

corners = shi_tomashi(img)
mat = cv2.getPerspectiveTransform(np.float32(corners), np.float32(destPoints))

warped = cv2.warpPerspective(img, mat, (900, 900))

data = np.uint8([[0]*900]*900)
data = cv2.circle(data, (450, 750), 90, (255,0,0), 5)

warpedData = cv2.warpPerspective(data, np.linalg.inv(mat), (img.shape[1], img.shape[0]))

plt.figure(1)
plt.subplot(1, 2, 1)
plt.imshow(img, cmap='gray')

plt.subplot(1,2,2)
# plt.imshow(warped, cmap='gray')
# plt.imshow(warpedData, cmap='gray')
plt.imshow(img+warpedData, cmap='gray')

plt.figure(3)
plt.imshow(warped, cmap='gray')

plt.show()