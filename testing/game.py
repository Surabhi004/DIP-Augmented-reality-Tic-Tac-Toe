import numpy as np

def display(board):
    print(" ")
    print("  "+str(board[0][0])+"  |  "+str(board[0][1])+"  |  "+str(board[0][2])+"  ")
    print("-----|-----|-----")
    print("  "+str(board[1][0])+"  |  "+str(board[1][1])+"  |  "+str(board[1][2])+"  ")
    print("-----|-----|-----")
    print("  "+str(board[2][0])+"  |  "+str(board[2][1])+"  |  "+str(board[2][2])+"  ")
    
def input_board():
    entries = list(map(int, input().split()))
    board = np.array(entries).reshape(3, 3)
    return board

def fin(board):
    winpos = [[0,1,2],[3,4,5],[6,7,8],[0,3,6],[1,4,7],[2,5,8],[0,4,8],[2,4,6]]
    for p in winpos:
        if(board[int(p[0]/3)][int(p[0]%3)] == 1 and board[int(p[1]/3)][int(p[1]%3)] == 1 and board[int(p[2]/3)][int(p[2]%3)] == 1):
            print("YOU WON")
            print("Final board")
            display(board)
            return True
        elif(board[int(p[0]/3)][int(p[0]%3)] == 2 and board[int(p[1]/3)][int(p[1]%3)] == 2 and board[int(p[2]/3)][int(p[2]%3)] == 2):
            print("YOU LOSE")
            print("Final board")
            display(board)
            return True
    if 0 not in board:
        print("DRAW") 
        print("Final board")
        display(board)
        return True
    return False       


def cpuwinmove(i,board):
    b = board.copy()
    b[i] = 2
    winpos = [[0,1,2],[3,4,5],[6,7,8],[0,3,6],[1,4,7],[2,5,8],[0,4,8],[2,4,6]]
    for p in winpos:
        if(b[p[0]] == b[p[1]] == b[p[2]] == 2): 
            return True
    return False

def humanwinmove(i,board):
    b = board.copy()
    b[i] = 1
    winpos = [[0,1,2],[3,4,5],[6,7,8],[0,3,6],[1,4,7],[2,5,8],[0,4,8],[2,4,6]]
    for p in winpos:
        if(b[p[0]] == b[p[1]] == b[p[2]] == 1): 
            return True

    return False
def computer_move(board):
    b = np.ravel(board)
    for i in range(len(b)):
        if(b[i] == 0 and cpuwinmove(i,b)):
            return i
    for i in range(len(b)):
        if(b[i] == 0 and humanwinmove(i,b)):
            return i
    for i in [4,0,7,2,6,1,3,5,7]:
        if(b[i] == 0):
            return i        

def nextmove(board):
    i = computer_move(board)
    nextboard = board.copy()
    nextboard[int(i/3)][int(i%3)] = 2
    return nextboard

def game(board):
    display(board)
    if fin(board):
        print("Game is finished")
    else:
        board = nextmove(board)
        display(board)
        if fin(board):
            print("Game is finished")
        else:
            board = input_board()
            game(board)

b = input_board()
game(b)
